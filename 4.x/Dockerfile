FROM openshift/base-centos7

# This image provides a Node.JS environment you can use to run your Node.JS
# applications.

MAINTAINER BouncingPixel <mhall@bouncingpixel.com>

EXPOSE 8080

ENV NODEJS_VERSION 4.x

LABEL io.k8s.description="Platform for building and running Node.js 4.x applications" \
      io.k8s.display-name="Node.js 4.x" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,nodejs,nodejs4x" \
      com.redhat.dev-mode="DEV_MODE:false" \
      com.redhat.deployments-dir="/opt/app-root/src" \
      com.redhat.dev-mode.port="DEBUG_PORT:5858"

RUN curl --silent --location https://rpm.nodesource.com/setup_4.x | bash -

RUN INSTALL_PKGS="nodejs-4.6.0" && \
    yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
RUN echo "scripts version 1.7"
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Each language image can have 'contrib' a directory with extra files needed to
# run and build the applications.
COPY ./contrib/ /opt/app-root

RUN git config --system user.name Openshift && git config --system user.email openshift@localhost

RUN $PROMPT_COMMAND && npm install -g supervisor grunt-cli bower gulp

# Drop the root user and make the content of /opt/app-root owned by user 1001
RUN chown -R 1001:0 /opt/app-root
USER 1001

# Set the default CMD to print the usage of the language image
CMD $STI_SCRIPTS_PATH/usage
