var fs = require('fs');
var fsReadFileAsync = fs.readFile;
var path = require('path');

var cache = {};
var cacheLength = 333; // in millis

fs.readFile = function(filename, encoding, callback) {
  if (arguments.length < 2) {
    throw new Error('Must include filename and callback at a minimum');
  }
  if (arguments.length === 2) {
    if (typeof encoding === 'function' || encoding instanceof Function) {
      callback = encoding;
      encoding = undefined;
    } else {
      throw new TypeError('Callback must be a function');
    }
  }

  // use setImmediate to make sure it does go async on us
  setImmediate(function() {
    var isDust = filename.length > 5 && filename.substring(filename.length - 5) === '.dust';
    var nowMillis = new Date().getTime();

    if (isDust && cache[filename] && cache[filename].expire >= nowMillis) {
      callback(null, cache[filename].file);
      return;
    }

    try {
      var str = fs.readFileSync(filename, encoding);
      if (isDust) {
        cache[filename] = {
          file: str,
          expire: nowMillis + cacheLength
        };
      }

      callback(null, str);
    } catch (e) {
      callback(e);
    }
 });
};

require(path.resolve('./', process.argv[2]));

